import React, { Component } from 'react'
import Head from 'next/head'
import CartDetail from 'components/cart'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        <Head>
          <title>Checkout</title>
        </Head>

        <section className='page-title'>
          <h2>Checkout</h2>
        </section>

        <section className='page-content'>
          <CartDetail />
        </section>

      </React.Fragment>
    )
  }
}
