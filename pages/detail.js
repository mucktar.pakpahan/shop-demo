import React, { Component } from 'react'
import Head from 'next/head'
import Breadcrumbs from 'components/common/breadcrumb'
import ProductDetail from 'components/product/detail'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        <Head>
          <link rel='stylesheet' type='text/css' charSet='UTF-8' href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css' />
        </Head>

        <section className='page-content'>
          <div className='container'>
            <Breadcrumbs />
          </div>

          <ProductDetail />
        </section>

      </React.Fragment>
    )
  }
}
