import React, { Component } from 'react'
import ProductList from 'components/product'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        {/* [Highligt Product] */}
        <section className='page-title'>
          <h2>Women</h2>
          <p className='description'>
            New Arrivals Women Collection 2018
          </p>
        </section>

        {/* [ Product List] */}
        <section className='page-content'>
          <div className='container'>
            <div className='row'>
              <div className='col-sm-12 col-md-12 products'>
                <ProductList />

                <div className='pagination'>
                  <div className='pagination-wrap'>
                    <a href='#' className='item-pagination active-pagination'>Load More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    )
  }
}
