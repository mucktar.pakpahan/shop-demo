import React, { Component } from 'react'
import Head from 'next/head'
import AccountSidebar from 'components/customer/sidebar'
import OrderDetail from 'components/order/detail'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        <Head>
          <title>Order Detail</title>
        </Head>

        <section className='page-content bg-grey'>
          <div className='container'>
            <div className='row'>
              <div className='col-sm-6 col-md-3 col-lg-2'>
                <AccountSidebar />
              </div>

              <div className='col-sm-6 col-md-9 col-lg-10'>
                <div className='content-title'>
                  <h1 className='content-header'>Order Tracking</h1>
                </div>
                <div className='products boxed'>
                  <OrderDetail />
                </div>
              </div>
            </div>
          </div>
        </section>

      </React.Fragment>
    )
  }
}
