import React, { Component } from 'react'
import Head from 'next/head'
import AccountSidebar from 'components/account/sidebar'
import OrderList from 'components/order'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        <Head>
          <title>Order History</title>
        </Head>

        <section className='page-content bg-grey'>
          <div className='container'>
            <div className='row'>
              <div className='col-sm-6 col-md-3 col-lg-2'>
                <AccountSidebar />
              </div>

              <div className='col-sm-6 col-md-9 col-lg-10'>
                <div className='content-title'>
                  <h1 className='content-header'>Orders</h1>
                </div>

                <div className='boxed'>
                  <OrderList />
                </div>

              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    )
  }
}
