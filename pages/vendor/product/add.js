import React, { Component } from 'react'
import Head from 'next/head'
import AccountSidebar from 'components/account/sidebar'
import AddProduct from 'components/product/add'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        <Head>
          <title>Add New Products</title>
        </Head>

        <section>
          <div className='page-content bg-grey'>
            <div className='container'>
              <div className='row'>
                <div className='col-sm-6 col-md-3 col-lg-2'>
                  <AccountSidebar />
                </div>

                <div className='col-sm-6 col-md-9 col-lg-10'>
                  <div className='content-title'>
                    <h1 className='content-header'>Add New Product</h1>
                  </div>

                  <div className='boxed'>
                    <AddProduct />
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    )
  }
}
