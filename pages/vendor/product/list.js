import React, { Component } from 'react'
import Head from 'next/head'
import AccountSidebar from 'components/account/sidebar'
import ProductTable from 'components/product/table'

export default class extends Component {
  render () {
    return (
      <React.Fragment>
        <Head>
          <title>Products</title>
        </Head>

        <section>
          <div className='page-content bg-grey'>
            <div className='container'>
              <div className='row'>
                <div className='col-sm-6 col-md-3 col-lg-2'>
                  <AccountSidebar />
                </div>

                <div className='col-sm-6 col-md-9 col-lg-10'>
                  <div className='content-title'>
                    <div className='content-action'>
                      <div className='btn-group'>
                        <a href='/vendor/product/add' className='btn-dark'>
                          <i className='fa fa fa-cloud-download' />
                          Add Product
                        </a>
                      </div>
                    </div>
                    <h1 className='content-header'>Products</h1>
                  </div>

                  <div className='boxed'>
                    <ProductTable />
                  </div>

                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    )
  }
}
