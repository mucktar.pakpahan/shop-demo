const path = require('path')
const express = require('express')
const session = require('express-session')
const compression = require('compression')
const next = require('next')
const helmet = require('helmet')
const cookieParser = require('cookie-parser')

const routes = require('./routes')

const port = parseInt(process.env.PORT, 10) || 3100
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })

const handler = routes.getRequestHandler(app)

app.prepare().then(() => {
  const server = express()

  server.use(helmet())
  server.use(compression())
  // initialize cookie-parser to allow us access the cookies stored in the browser.
  server.use(cookieParser())
  // use sessions for tracking logins
  server.use(session({
    key: 'user_sid',
    secret: process.env.SECRET || 'secret',
    resave: true,
    saveUninitialized: false,
    cookie: {
      expires: 600000
    }
  }))

  // This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
  // This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
  server.use((req, res, next) => {
    if (req.cookies.user_sid && !req.session.user) {
      res.clearCookie('user_sid')
    }

    next()
  })

  const staticPath = path.join(__dirname, '../static')

  server.use('/static', express.static(staticPath, {
    maxAge: '30d',
    immutable: true
  }))

  // middleware function to check for logged-in users
  const sessionChecker = (req, res, next) => {
    if (!req.session.user && !req.cookies.user_sid) {
      res.redirect('/')
    } else {
      next()
    }
  }

  server.get('/customer/*', (req, res) => {
    return handler(req, res)
  })

  server.get('/vendor/*', (req, res) => {
    return handler(req, res)
  })

  server.get('/check-if-login/*', sessionChecker, (req, res) => {
    return handler(req, res)
  })

  server.get('*', (req, res) => {
    return handler(req, res)
  })

  startServer()

  function startServer () {
    server.listen(port, () => {
      console.log(`> Ready on http://localhost:${port}`)
    })
  }
})
