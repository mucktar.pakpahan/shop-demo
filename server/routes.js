const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

const APP_ROUTES = [{
  page: 'index',
  pattern: '/'
}, {
  page: 'detail',
  pattern: '/product/:slug'
}, {
  page: 'cart',
  pattern: '/cart'
}, {
  page: 'checkout',
  pattern: '/checkout'
}, {
  page: 'order/history',
  pattern: '/customer/order'
}, {
  page: 'order/view',
  pattern: '/customer/order/:id'
}, {
  page: 'vendor/order/list',
  pattern: '/vendor/order'
}, {
  page: 'vendor/order/detail',
  pattern: '/vendor/order/:id'
}, {
  page: 'vendor/product/list',
  pattern: '/vendor/product'
}, {
  page: 'vendor/product/add',
  pattern: '/vendor/product/add'
}, {
  page: 'vendor/product/detail',
  pattern: '/vendor/product/:id'
}]

APP_ROUTES.forEach(route => routes.add(route))
