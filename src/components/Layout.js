import React, { PureComponent } from 'react'
import Header from 'components/header'
import Footer from 'components/footer'

import 'styles/main.scss'
import 'bootstrap/dist/css/bootstrap.min.css'

export default class Layout extends PureComponent {
  render () {
    return (
      <React.Fragment>
        <Header />
        <section className='content'>
          { this.props.children }
        </section>
        <Footer />
      </React.Fragment>
    )
  }
}
