import React from 'react'
import {
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon
} from 'reactstrap'

class LoginForm extends React.Component {
  constructor (props) {
    super(props)

    this.state = {

    }
  }

  handleSubmit = (e) => {
    this.props.onSubmit()

    e.preventDefault()
  }

  render () {
    return (
      <div className='popup'>
        <div className='popup-content'>
          <div className='popup-header'>
            <img src='/static/images/icons/logo.png' alt='logo' />
            <h2>Sign In to your account</h2>
          </div>
          <div className='popup-body'>
            <Form>
              <FormGroup>
                <Label for='product_name'>Email</Label>
                <InputGroup>
                  <InputGroupAddon addonType='prepend'>
                    <i className='fa fa-user' />
                  </InputGroupAddon>
                  <Input type='email' placeholder='Ex: mail@example.com' />
                </InputGroup>
              </FormGroup>

              <FormGroup>
                <Label for='product_name'>Password</Label>
                <InputGroup>
                  <InputGroupAddon addonType='prepend'>
                    <i className='fa fa-asterisk' />
                  </InputGroupAddon>
                  <Input type='password' placeholder='********' />
                </InputGroup>
              </FormGroup>

              <FormGroup>
                <button className='btn-dark btn-submit' type='submit' onClick={this.handleSubmit}>Sign In</button>
              </FormGroup>
            </Form>
          </div>
        </div>
        <div className='pad-ver'>
          <a href='#' onClick={this.props.showRegisterForm}>Create a new account</a>
        </div>
      </div>
    )
  }
}

export default LoginForm
