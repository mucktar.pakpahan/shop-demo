import React from 'react'
import {
  Form,
  FormGroup,
  Label,
  Input,
  InputGroup,
  InputGroupAddon
} from 'reactstrap'

export default (props) => (
  <div className='popup'>
    <div className='popup-content'>
      <div className='popup-header'>
        <img src='/static/images/icons/logo.png' alt='logo' />
        <h2>Create New Account</h2>
      </div>
      <div className='popup-body'>
        <Form>
          <FormGroup>
            <Label for='product_name'>Username</Label>
            <InputGroup>
              <InputGroupAddon addonType='prepend'>
                <i className='fa fa-user' />
              </InputGroupAddon>
              <Input type='email' placeholder='Ex: mail@example.com' />
            </InputGroup>
          </FormGroup>

          <FormGroup>
            <Label for='product_name'>Email</Label>
            <InputGroup>
              <InputGroupAddon addonType='prepend'>
                <i className='fa fa-envelope' />
              </InputGroupAddon>
              <Input type='email' placeholder='Ex: mail@example.com' />
            </InputGroup>
          </FormGroup>

          <FormGroup>
            <Label for='product_name'>Password</Label>
            <InputGroup>
              <InputGroupAddon addonType='prepend'>
                <i className='fa fa-asterisk' />
              </InputGroupAddon>
              <Input type='password' placeholder='********' />
            </InputGroup>
          </FormGroup>

          <FormGroup>
            <button className='btn-dark btn-submit' type='submit'>Register</button>
          </FormGroup>
        </Form>
      </div>
    </div>
    <div>
      Already have an account ? <a href='#' onClick={props.showLoginForm}>Sign In</a>
    </div>
  </div>
)
