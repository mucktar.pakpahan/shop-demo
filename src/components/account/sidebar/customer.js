import React from 'react'

export default () => (
  <React.Fragment>
    <li>
      <a href='/customer/order' className='active'>
        Order History
      </a>
    </li>
  </React.Fragment>
)
