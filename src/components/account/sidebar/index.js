import React from 'react'
import CustomerMenu from './customer'
import VendorMenu from './vendor'

class Sidebar extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='leftbar'>
        <h4 className='categories'>
          My Account
        </h4>

        <ul>
          <li>
            <a href='#'>
              Account Detail
            </a>
          </li>

          { this.props.accountType === 'customer'
            ? <CustomerMenu />
            : <VendorMenu />
          }
        </ul>
      </div>
    )
  }
}

/**
 * Default Props
 */
Sidebar.defaultProps = {
  accountType: 'customer'
}

export default Sidebar
