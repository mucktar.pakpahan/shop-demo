import React from 'react'

export default () => (
  <React.Fragment>
    <li>
      <a href='/vendor/order' className='active'>
        Orders
      </a>
    </li>
    <li>
      <a href='/vendor/product' className='active'>
        Products
      </a>
    </li>
  </React.Fragment>
)
