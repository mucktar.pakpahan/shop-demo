import React from 'react'
import { Col, Row, FormGroup, Label, Input, InputGroup, InputGroupText, InputGroupAddon } from 'reactstrap'

class Availibility extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      availibility: [{
        start: '00:00',
        end: '23:59',
        value: 17
      }]
    }
  }

  addHourlyWatt = () => {
    const joined = this.state.availibility.concat({
      start: '',
      end: '',
      value: 0
    })

    this.setState({ availibility: joined })
  }

  changeStart = () => {

  }

  changeEnd = () => {

  }

  changeValue = () => {

  }

  showHourlyWatts = () => {
    return this.state.availibility.map((item, i) => {
      return (
        <Row form className='availibility-hours' key={i}>
          <Col md={6}>
            <FormGroup>
              <Label for='hour'>Hour</Label>
              <InputGroup>
                <InputGroupAddon addonType='prepend'>
                  <Input className='hour hour-start' placeholder='From'
                    onChange={this.changeStart}
                    value={item.start} />
                </InputGroupAddon>

                <InputGroupText>
                  <i className='fa fa-arrow-right' aria-hidden='true' />
                </InputGroupText>

                <InputGroupAddon addonType='append'>
                  <Input className='hour hour-end' placeholder='Until'
                    onChange={this.changeEnd}
                    value={item.end} />
                </InputGroupAddon>
              </InputGroup>
            </FormGroup>
          </Col>
          <Col md={6}>
            <FormGroup>
              <Label for='amount'>Amount</Label>
              <Input className='amount' type='text' name='amount[]'
                onChange={this.changeValue}
                value={item.value} />
            </FormGroup>
          </Col>
        </Row>
      )
    })
  }

  render = () => (
    <div className='container-availibility'>
      <div>
        <h5 className='cart-total-title'>
          Hourly Availibility
        </h5>
        <small>
          Specify hourly watts needed.
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </small>
      </div>

      { this.showHourlyWatts() }

      <div className='availibility-action'>
        <div className='add-availibility-hour'>
          <button className='btn-dark' onClick={this.addHourlyWatt}>
            <i className='fa fa-plus' aria-hidden='true' />
          </button>
        </div>
      </div>
    </div>
  )
}

export default Availibility
