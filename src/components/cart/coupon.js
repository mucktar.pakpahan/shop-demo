import React from 'react'

export default () => (
  <div className='coupon w-full-sm'>
    <div className='coupon-input-wrapper'>
      <input className='coupon-code' type='text' name='coupon-code' placeholder='Coupon Code' />
    </div>

    <div className='btn-apply-coupon-wrapper'>
      <button className='btn-dark'>
        Apply coupon
      </button>
    </div>
  </div>
)
