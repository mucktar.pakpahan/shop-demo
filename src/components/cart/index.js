import React from 'react'
import CartTable from 'components/cart/table'
import Summary from 'components/cart/summary'
import Availibility from 'components/cart/availibility'

class Cart extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <section className='cart'>
        <div className='container'>
          <div className='container-table-cart pos-relative'>
            <div className='wrap-table-shopping-cart'>
              <CartTable />
              <Availibility />
            </div>

            <Summary />
          </div>

          {/*
          <div className='container-coupon p-lr-15-sm'>
            <Coupon />

            <div className='btn-update-cart'>
              <button className='btn-dark'>
                Update Cart
              </button>
            </div>
          </div>
          */}

        </div>
      </section>
    )
  }
}

export default Cart
