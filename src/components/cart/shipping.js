import React from 'react'
import Select from 'react-select'

const countries = [
  { value: 'US', label: 'US' },
  { value: 'UK', label: 'UK' },
  { value: 'Japan', label: 'Japan' }
]

class CalcShipping extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      selectedCountry: null
    }
  }

  handleChange = (selectedCountry) => {
    this.setState({ selectedCountry })
  }

  render () {
    const { selectedCountry } = this.state

    return (
      <React.Fragment>
        <p className='calc-shipping-info'>
          There are no shipping methods available. Please double check your address, or contact us if you need any help.
        </p>

        <span className='calc-shipping-title'>
          Calculate Shipping
        </span>

        <div className='calc-shipping-country rs2-select2 rs3-select2 rs4-select2'>
          <Select
            instanceId='shipping_country'
            className='select2 shipping-country'
            value={selectedCountry}
            onChange={this.handleChange}
            options={countries}
            placeholder='Select a country...'
          />
        </div>

        <div className='calc-shipping-state'>
          <input className='input-default' type='text' name='state' placeholder='State /  country' />
        </div>

        <div className='calc-shipping-zip'>
          <input className='input-default' type='text' name='postcode' placeholder='Postcode / Zip' />
        </div>

        <div className='calc-shipping-update'>
          <button className='btn-dark'>
            Update Totals
          </button>
        </div>
      </React.Fragment>
    )
  }
}

export default CalcShipping
