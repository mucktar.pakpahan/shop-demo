import React from 'react'

export default () => (
  <div className='container-cart-total p-lr-15-sm'>
    <h5 className='cart-total-title'>
      Cart Totals
    </h5>

    <div className='subtotal'>
      <span className='subtotal-title w-full-sm'>
        Subtotal:
      </span>

      <span className='subtotal-price w-full-sm'>
        $612.00
      </span>
    </div>

    <div className='total'>
      <span className='total-title w-full-sm'>
        Total:
      </span>

      <span className='total-price w-full-sm'>
        $612.00
      </span>
    </div>

    <div className='btn-checkout size15 trans-0-4'>
      <button className='btn-dark'>
        Confirm
      </button>
    </div>
  </div>
)
