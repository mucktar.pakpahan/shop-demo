import React from 'react'

class CartTable extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  increaseCartQuantity = () => {

  }

  decreaseCartQuantity = () => {

  }

  render () {
    return (
      <table className='table-shopping-cart'>
        <thead>
          <tr className='table-head'>
            <th className='column-1' >Product</th>
            <th className='column-2' />
            <th className='column-3'>Price/KW</th>
            <th className='column-4 p-l-70'>Quantity(in KW)</th>
            <th className='column-5'>Total</th>
          </tr>
        </thead>
        <tbody>
          <tr className='table-row'>
            <td className='column-1'>
              <div className='cart-img-product b-rad-4'>
                <img src='/static/images/item-cart-01.jpg' alt='IMG-PRODUCT' />
              </div>
            </td>
            <td className='column-2'>Men Tshirt</td>
            <td className='column-3'>$36.00</td>
            <td className='column-4'>
              17 Kw
            </td>
            <td className='column-5'>$612.00</td>
          </tr>
        </tbody>
      </table>
    )
  }
}

export default CartTable
