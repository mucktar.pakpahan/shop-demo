import React from 'react'

class Breadcrumb extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='bread-crumb p-l-15-sm'>
        <a href='/'>
          Home
          <i className='fa fa-angle-right' aria-hidden='true' />
        </a>

        <a href='/'>
          Women
          <i className='fa fa-angle-right' aria-hidden='true' />
        </a>

        <a href='/'>
          T-Shirt
          <i className='fa fa-angle-right' aria-hidden='true' />
        </a>

        <span className='active'>
          Boxy T-Shirt with Roll Sleeve Detail
        </span>
      </div>
    )
  }
}

export default Breadcrumb
