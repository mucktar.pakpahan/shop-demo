import React from 'react'

export default (props) => (
  <div className='counter'>
    <button className='btn-num increase' onClick={props.decrease}>
      <i className='fs-12 fa fa-minus' aria-hidden='true' />
    </button>

    <div className='num-product'>{props.total}</div>

    <button className='btn-num decrease' onClick={props.increase}>
      <i className='fs-12 fa fa-plus' aria-hidden='true' />
    </button>
  </div>
)
