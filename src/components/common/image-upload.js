import React from 'react'
import Dropzone from 'react-dropzone'

class ImageUpload extends React.Component {
  constructor () {
    super()
    this.state = {
      files: []
    }
  }

  onDrop (files) {
    this.setState({
      files: files.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      }))
    })
  }

  componentWillUnmount () {
    // Make sure to revoke the data uris to avoid memory leaks
    this.state.files.forEach(file => URL.revokeObjectURL(file.preview))
  }

  render () {
    const { files } = this.state

    const thumbs = files.map(file => (
      <div key={file.name} className='thumb'>
        <div className='thumb-inner'>
          <img className='thumb-img'
            src={file.preview}
          />
        </div>
      </div>
    ))

    return (
      <section className='image-upload'>
        <Dropzone className='upload-container'
          accept='image/*'
          onDrop={this.onDrop.bind(this)}
        >
          {({ getRootProps, getInputProps }) => (
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              <div className='upload-message'>
                <p>Drop files here or click to upload.</p>
              </div>
            </div>
          )}
        </Dropzone>
        <aside className='thumb-container'>
          {thumbs}
        </aside>
      </section>
    )
  }
}

export default ImageUpload
