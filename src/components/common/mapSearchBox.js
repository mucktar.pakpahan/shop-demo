import React, { Component } from 'react'
import PropTypes from 'prop-types'

class SearchBox extends Component {
  static propTypes = {
    mapApi: PropTypes.shape({
      places: PropTypes.shape({
        SearchBox: PropTypes.func
      }),
      event: PropTypes.shape({
        clearInstanceListeners: PropTypes.func
      })
    }).isRequired,
    placeholder: PropTypes.string,
    onPlacesChanged: PropTypes.func
  };

  static defaultProps = {
    placeholder: 'Search...',
    onPlacesChanged: null
  };

  componentDidMount ({ map, mapApi } = this.props) {
    this.searchBox = new mapApi.places.SearchBox(this.searchInput)
    this.searchBox.addListener('places_changed', this.onPlacesChanged)
    this.searchBox.bindTo('bounds', map)
  }

  componentWillUnmount ({ mapApi } = this.props) {
    mapApi.event.clearInstanceListeners(this.searchInput)
  }

  onPlacesChanged = ({ map, addplace } = this.props) => {
    const selected = this.searchBox.getPlaces()
    const { 0: place } = selected

    if (!place.geometry) return
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport)
    } else {
      map.setCenter(place.geometry.location)
      map.setZoom(17)
    }

    addplace(selected)
    this.searchInput.blur()
  };

  render () {
    const { placeholder } = this.props

    return (
      <div className='map-search-wrapper form-group'>
        <input className='map-search form-control'
          ref={(ref) => {
            this.searchInput = ref
          }}
          type='text'
          placeholder={placeholder}
        />
      </div>
    )
  }
}

export default SearchBox
