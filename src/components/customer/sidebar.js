import React from 'react'

class Sidebar extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='leftbar'>
        <h4 className='categories'>
          My Account
        </h4>

        <ul>
          <li>
            <a href='#'>
              Account Detail
            </a>
          </li>

          <li>
            <a href='#' className='active'>
              Order History
            </a>
          </li>
        </ul>
      </div>
    )
  }
}

export default Sidebar
