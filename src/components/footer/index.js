import React from 'react'

export default class Footer extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <footer className='page-footer'>
        <div className='page-footer-wrapper'>
          <div className='item'>
            <h4>GET IN TOUCH</h4>

            <div className='contact'>
              <p>
                Any questions? Let us know in store at 8th floor, 379 Hudson St, New York, NY 10018 or call us on (+1) 96 716 6879
              </p>

              <div className='social'>
                <a href='#' className='fa fa-facebook' />
                <a href='#' className='fa fa-instagram' />
                <a href='#' className='fa fa-pinterest-p' />
                <a href='#' className='fa fa-snapchat-ghost' />
                <a href='#' className='fa fa-youtube-play' />
              </div>
            </div>
          </div>

          <div className='item'>
            <h4>Categories</h4>

            <ul className='categories'>
              <li>
                <a href='#'>Men</a>
              </li>

              <li>
                <a href='#'>Women</a>
              </li>

              <li>
                <a href='#'>Dresses</a>
              </li>

              <li>
                <a href='#'>Sunglasses</a>
              </li>
            </ul>
          </div>

          <div className='item'>
            <h4>Links</h4>

            <ul className='links'>
              <li>
                <a href='#'>Search</a>
              </li>

              <li>
                <a href='#'>About Us</a>
              </li>

              <li>
                <a href='#'>Contact Us</a>
              </li>

              <li>
                <a href='#'>Returns</a>
              </li>
            </ul>
          </div>

          <div className='item'>
            <h4>Help</h4>

            <ul className='help'>
              <li>
                <a href='/customer/order'>Track Order</a>
              </li>

              <li>
                <a href='#'>Returns</a>
              </li>

              <li>
                <a href='#'>Shipping</a>
              </li>

              <li>
                <a href='#'>FAQs</a>
              </li>
            </ul>
          </div>

          <div className='item'>
            <h4>Newsletter</h4>

            <form>
              <div className='newsletter effect1 w-size9'>
                <input className='s-text7 bg6 w-full p-b-5' type='text' name='email' placeholder='email@example.com' />
                <span className='effect1-line' />
              </div>

              <div className='btn-subscribe w-size2 p-t-20'>
                <button className='btn-dark flex-c-m size2 bg4 bo-rad-23 hov1 m-text3 trans-0-4'>
                  Subscribe
                </button>
              </div>

            </form>
          </div>
        </div>

        <div className='payment-method t-center p-l-15 p-r-15'>
          <a href='#'>
            <img src='/static/images/icons/paypal.png' alt='IMG-PAYPAL' />
          </a>

          <a href='#'>
            <img src='/static/images/icons/visa.png' alt='IMG-VISA' />
          </a>

          <a href='#'>
            <img src='/static/images/icons/mastercard.png' alt='IMG-MASTERCARD' />
          </a>

          <a href='#'>
            <img src='/static/images/icons/express.png' alt='IMG-EXPRESS' />
          </a>

          <a href='#'>
            <img src='/static/images/icons/discover.png' alt='IMG-DISCOVER' />
          </a>

          <div className='t-center s-text8 p-t-20'>
            Copyright © 2018 All rights reserved. | This template is made with <i className='fa fa-heart-o' aria-hidden='true' /> by <a href='https://colorlib.com' target='_blank'>Colorlib</a>
          </div>
        </div>
      </footer>
    )
  }
}
