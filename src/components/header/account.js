import React from 'react'
import CustomerMenu from 'components/account/sidebar/customer'
import VendorMenu from 'components/account/sidebar/vendor'

const AccountMenu = (props) => (
  <div className={`header-cart sub-menu`}>
    <ul className='menu-list'>
      <CustomerMenu />
      <VendorMenu />
    </ul>
  </div>
)

/**
 * Default Props
 */
AccountMenu.defaultProps = {
  isVendor: false
}

export default AccountMenu
