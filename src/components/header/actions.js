import React from 'react'

export default (props) => (
  <div className='header-actions'>
    <div className='header-actions-wrap'>
      <a href='#' onClick={props.showLoginForm} className='btn-outlined btn-dark btn-login pull-left'>
        Login
      </a>
    </div>
    <div className='header-actions-wrap'>
      <a href='#' onClick={props.showRegisterForm} className='btn-outlined btn-dark btn-signup pull-right'>
        Signup
      </a>
    </div>
  </div>
)
