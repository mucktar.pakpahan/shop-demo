import React from 'react'

export default () => (
  <div className='header-cart-item'>
    <div className='header-cart-item-img'>
      <img src='/static/images/item-cart-01.jpg' alt='IMG' />
    </div>

    <div className='header-cart-item-txt'>
      <a href='#' className='header-cart-item-name'>
        White Shirt With Pleat Detail Back
      </a>

      <span className='header-cart-item-info'>
        1 x $19.00
      </span>
    </div>
  </div>
)
