import React from 'react'
import HeaderCartItem from './cart-item'

class HeaderCart extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    const active = this.props.isActive ? 'header-cart-show' : ''

    return (
      <div className={`header-cart ${active}`}>
        <ul className='header-cart-wrapitem'>
          <li>
            <HeaderCartItem />
          </li>

          <li className='header-cart-item'>
            <div className='header-cart-item-img'>
              <img src='/static/images/item-cart-02.jpg' alt='IMG' />
            </div>

            <div className='header-cart-item-txt'>
              <a href='#' className='header-cart-item-name'>
                Converse All Star Hi Black Canvas
              </a>

              <span className='header-cart-item-info'>
                1 x $39.00
              </span>
            </div>
          </li>

          <li className='header-cart-item'>
            <div className='header-cart-item-img'>
              <img src='/static/images/item-cart-03.jpg' alt='IMG' />
            </div>

            <div className='header-cart-item-txt'>
              <a href='#' className='header-cart-item-name'>
                Nixon Porter Leather Watch In Tan
              </a>

              <span className='header-cart-item-info'>
                1 x $17.00
              </span>
            </div>
          </li>
        </ul>

        <div className='header-cart-total'>
          Total: $75.00
        </div>

        <div className='header-cart-buttons'>
          <div className='header-cart-wrapbtn'>
            <a href='/cart' className='btn-dark'>
              View Cart
            </a>
          </div>

          <div className='header-cart-wrapbtn'>
            <a href='/checkout' className='btn-dark'>
              Check Out
            </a>
          </div>
        </div>
      </div>
    )
  }
}

/**
 * Default Props
 */
HeaderCart.defaultProps = {
  isActive: true
}

export default HeaderCart
