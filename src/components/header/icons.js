import React from 'react'
import AccountMenu from 'components/header/account'

export default (props) => (
  <div className='header-icons'>
    <div className='header-icons-wrap'>
      <span className='profile-name'>
        {props.user.name}
      </span>
      <img src={props.user.image} alt={props.user.name}
        onClick={props.toggle} />

      <AccountMenu
        isActive={props.active}
        cartToggle={props.toggle}
      />
    </div>
  </div>
)
