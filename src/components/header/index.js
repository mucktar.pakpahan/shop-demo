import React from 'react'
import Link from 'next/link'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'

import HeaderActions from 'components/header/actions'
import HeaderIcons from 'components/header/icons'
import LoginForm from 'components/account/login'
import RegisterForm from 'components/account/register'

export default class Header extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      cartIsActive: false,
      isLogin: false,
      modalLogin: false,
      modalRegister: false,
      user: {
        name: 'John Doe',
        image: '/static/images/icons/icon-header-01.png'
      }
    }
  }

  cartToggle = () => {
    this.setState({
      cartIsActive: !this.state.cartIsActive
    })
  };

  toggleModal = () => {
    this.setState({
      modalLogin: false,
      modalRegister: false
    })
  }

  toggleLoginForm = () => {
    this.setState({
      modalLogin: !this.state.modalLogin,
      modalRegister: false
    })
  }

  toggleRegisterForm = () => {
    this.setState({
      modalLogin: false,
      modalRegister: !this.state.modalRegister
    })
  }

  login = () => {
    this.setState({
      isLogin: true
    })

    this.toggleModal()
  }

  render () {
    return (
      <header className='primary-header'>
        <div className='container-menu'>
          <div className='container'>
            <div className='wrap-header'>
              { /* -- Logo -- */ }
              <Link href='/'>
                <a className='logo'>
                  <img src='/static/images/icons/logo.png' alt='IMG-LOGO' />
                </a>
              </Link>

              { this.state.isLogin
                ? (
                  <HeaderIcons
                    user={this.state.user}
                    toggle={this.accountToggle}
                    active={this.state.accountIsActive}
                  />
                )
                : (
                  <HeaderActions
                    showLoginForm={this.toggleLoginForm}
                    showRegisterForm={this.toggleRegisterForm}
                  />
                )
              }
            </div>
          </div>
        </div>

        <Modal isOpen={this.state.modalLogin || this.state.modalRegister} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal} />
          <ModalBody>
            { this.state.modalLogin
              ? (
                <LoginForm
                  onSubmit={this.login}
                  showRegisterForm={this.toggleRegisterForm}
                />
              )
              : ''
            }

            { this.state.modalRegister
              ? (
                <RegisterForm
                  onSubmit={this.register}
                  showLoginForm={this.toggleLoginForm}
                />
              )
              : ''
            }

          </ModalBody>
        </Modal>
      </header>
    )
  }
}
