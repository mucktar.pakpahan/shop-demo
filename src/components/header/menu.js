import React from 'react'
import Link from 'next/link'

export default () => (
  <nav className='menu'>
    <ul className='main-menu'>
      <li>
        <Link href='/'>
          <a>Home</a>
        </Link>
      </li>
      <li>
        <Link href='/product'>
          <a>Shop</a>
        </Link>
      </li>

      <li className='sale-noti'>
        <a href='/'>Sale</a>
      </li>

      <li>
        <a href='/'>Features</a>
      </li>

      <li>
        <a href='/'>Blog</a>
      </li>

      <li>
        <a href='/'>About</a>
      </li>

      <li>
        <a href='/'>Contact</a>
      </li>
    </ul>
  </nav>
)
