import React from 'react'

class OrderDetail extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='order-detail'>
        <div className='row'>
          <div className='wrap-order-detail'>
            <div className='order-tracking'>
              <div className='title'>
                  Order #453234
              </div>
              <button className='btn-dark btn-shipped'>
                Set as Shipped
              </button>

            </div>

            <div className='order-account'>
              <div className='unit'>
                <div className='box'>
                  <div className='account-info'>
                    Shipping Address
                  </div>
                  <div className='account-address'>
                    <label>Mucktar Pakpahan, 085212217930</label>
                    <p>
                      Jl. Karet Karya 2 No. 19 A Kel. Karet Setiabudi, Setiabudi, Jakarta Selatan, DKI Jakarta, 12920
                    </p>
                  </div>
                </div>
              </div>
              <div className='unit'>
                <div className='box'>
                  <div className='account-info'>
                    Billing Address
                  </div>
                  <div className='account-address'>
                    <label>Mucktar Pakpahan, 085212217930</label>
                    <p>
                      Jl. Karet Karya 2 No. 19 A Kel. Karet Setiabudi, Setiabudi, Jakarta Selatan, DKI Jakarta, 12920
                    </p>
                  </div>
                </div>
              </div>
              <div className='unit'>
                <div className='account-info'>
                  Payment Method
                </div>
                <div className='account-address'>
                  <label>Bank Transfer</label>
                </div>
              </div>
            </div>

            <div className='order-tracking'>
              <div className='title'>
                Ordered Products
              </div>
            </div>

            <div className='order-history account'>
              <div className='container-table-cart pos-relative'>
                <div className='wrap-table-shopping-cart'>
                  <table className='table-shopping-cart'>
                    <thead>
                      <tr className='table-head'>
                        <th className='column-product'>Product</th>
                        <th className='column-status'>Status</th>
                        <th className='column-price'>Price</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr className='table-row'>
                        <td className='column-product'>
                          <div className='order-product'>
                            <div className='order-product-image'>
                              <img src='/static/images/item-cart-02.jpg' alt='IMG-PRODUCT' />
                            </div>

                            <div className='order-product-meta'>
                              <a className='product-name ' href='/' title='Gel-Vickka Trs'>
                                 Boxy T-Shirt with Roll Sleeve Detail
                              </a>
                              <div className='product-size'>
                                Size XL
                              </div>
                              <div className='product-price'>
                                $16.00
                              </div>
                              <div className='product-quantity'>
                                Quantity 1
                              </div>
                            </div>
                          </div>
                        </td>
                        <td className='column-status'>
                          <div className='order-status'>
                            <div className='orders-status-code-1'>
                              Completed
                            </div>

                            <div className='order-status-last-updated'>
                              Last Update:
                              2018-11-13 07:59
                            </div>
                          </div>
                        </td>
                        <td className='column-price'>
                          <div className='product-total-price'>
                            $16.00
                          </div>
                        </td>
                      </tr>

                      <tr className='table-row'>
                        <td className='column-product'>
                          <div className='order-product'>
                            <div className='order-product-image'>
                              <img src='/static/images/item-cart-02.jpg' alt='IMG-PRODUCT' />
                            </div>

                            <div className='order-product-meta'>
                              <a className='product-name ' href='/' title='Gel-Vickka Trs'>
                                 Boxy T-Shirt with Roll Sleeve Detail
                              </a>
                              <div className='product-size'>
                                Size XL
                              </div>
                              <div className='product-price'>
                                $16.00
                              </div>
                              <div className='product-quantity'>
                                Quantity 1
                              </div>
                            </div>
                          </div>
                        </td>
                        <td className='column-status'>
                          <div className='order-status'>
                            <div className='orders-status-code-1'>
                              Completed
                            </div>

                            <div className='order-status-last-updated'>
                              Last Update:
                              2018-11-13 07:59
                            </div>
                          </div>
                        </td>
                        <td className='column-price'>
                          <div className='product-total-price'>
                            $16.00
                          </div>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr className='shipping last'>
                        <td />
                        <td className='a-right'>
                          Shipping
                        </td>
                        <td className='last a-right'>
                          <span className='price'>$3.00</span>
                        </td>
                      </tr>
                      <tr className='last-row'>
                        <td />
                        <td className='a-right'>
                          <strong>Total</strong>
                        </td>
                        <td className='last a-right'>
                          <span className='txtNormal'>
                            <strong><span className='price'>$35.00</span></strong>
                          </span>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default OrderDetail
