import React from 'react'

class OrderHistory extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='order-history'>
        <div className='row'>
          <div className='container-table-cart pos-relative'>
            <div className='wrap-table-shopping-cart'>
              <table className='table-shopping-cart'>
                <thead>
                  <tr className='table-head'>
                    <th className='column-meta'>Order</th>
                    <th className='column-product'>Product</th>
                    <th className='column-status'>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className='table-row'>
                    <td className='column-meta'>
                      <div className='order-meta'>
                        <div className='order-number-container'>
                          <div className='order-number-description'>
                              Order Number
                          </div>
                          <div className='order-number'>
                              247516232
                          </div>
                        </div>
                        <div className='order-date'>
                          Ordered on 2018-10-17
                        </div>
                        <ul className='order-actions'>
                          <li className='order-action'>
                            <a className='btn-dark' href='/customer/order/order-id'>
                              view/edit order
                            </a>
                          </li>
                        </ul>
                      </div>
                    </td>
                    <td className='column-product'>
                      <div className='order-product'>
                        <div className='order-product-image'>
                          <img src='/static/images/item-cart-02.jpg' alt='IMG-PRODUCT' />
                        </div>

                        <div className='order-product-meta'>
                          <a className='product-name ' href='/' title='Gel-Vickka Trs'>
                             Boxy T-Shirt with Roll Sleeve Detail
                          </a>
                          <div className='product-size'>
                            Size XL
                          </div>
                          <div className='product-price'>
                            $16.00
                          </div>
                        </div>
                      </div>
                    </td>
                    <td className='column-status'>
                      <div className='order-status'>
                        <div className='orders-status-code-1'>
                          Completed
                        </div>

                        <div className='order-status-last-updated'>
                          Last Update:
                          2018-11-13 07:59
                        </div>
                      </div>
                    </td>
                  </tr>

                  <tr className='table-row'>
                    <td className='column-meta'>
                      <div className='order-meta'>
                        <div className='order-number-container'>
                          <div className='order-number-description'>
                            Order Number
                          </div>
                          <div className='order-number'>
                            247516232
                          </div>
                        </div>
                        <div className='order-date'>
                        Ordered on 2018-10-17
                        </div>
                        <ul className='order-actions'>
                          <li className='order-action'>
                            <a className='btn-dark' href='/customer/order/order-id'>
                            view/edit order
                            </a>
                          </li>
                        </ul>
                      </div>
                    </td>
                    <td className='column-product'>
                      <div className='order-product'>
                        <div className='order-product-image'>
                          <img src='/static/images/item-cart-02.jpg' alt='IMG-PRODUCT' />
                        </div>

                        <div className='order-product-meta'>
                          <a className='product-name ' href='/' title='Gel-Vickka Trs'>
                             Boxy T-Shirt with Roll Sleeve Detail
                          </a>
                          <div className='product-size'>
                            Size XL
                          </div>
                          <div className='product-price'>
                            $16.00
                          </div>
                        </div>
                      </div>
                    </td>
                    <td className='column-status'>
                      <div className='order-status'>
                        <div className='orders-status-code-1'>
                          Completed
                        </div>

                        <div className='order-status-last-updated'>
                          Last Update:
                          2018-11-13 07:59
                        </div>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default OrderHistory
