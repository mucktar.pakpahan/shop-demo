import React from 'react'
import Link from 'next/link'
import {
  Table,
  Nav,
  NavItem,
  NavLink,
  Pagination,
  PaginationItem,
  PaginationLink,
  InputGroupAddon,
  InputGroup,
  Input,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'

class OrderList extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      dropdownOpen: false
    }
  }

  toggleDropdown = () => {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    })
  }

  render () {
    return (
      <div className='panel'>
        <div className='panel-heading'>
          <Nav>
            <NavItem active>
              <NavLink href='#'>All</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href='#'>Open</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href='#'>Unpaid</NavLink>
            </NavItem>
          </Nav>
        </div>

        <div className='panel-body'>
          <div className='table-toolbar'>
            <div className='row'>
              <div className='col-sm-6 table-toolbar-left'>
                <InputGroup className='search-wrapper'>
                  <Input placeholder='Search product' type='text' step='1' />
                  <InputGroupAddon addonType='append'>
                    <button className='btn btn-search'>
                      <i className='fa fa-search' aria-hidden='true' />
                    </button>
                  </InputGroupAddon>
                </InputGroup>
              </div>
              <div className='col-sm-6 table-toolbar-right'>
                <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggleDropdown}>
                  <DropdownToggle caret color='default'>
                    <i className='fa fa-cog' />
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem>Cancel Order</DropdownItem>
                    <DropdownItem>Change to Shipped</DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
          </div>
          <Table>
            <thead>
              <tr>
                <th>Order</th>
                <th>Date</th>
                <th>Payment Status</th>
                <th>Order Status</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#1</a>
                  </Link>
                </th>
                <td>Feb 2, 10:00am</td>
                <td>
                  <span className='label label-default'>awaiting confirmation</span>
                </td>
                <td>
                  <span className='label label-default'>pending</span>
                </td>
                <td>$50.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#2</a>
                  </Link>
                </th>
                <td>Feb 2, 09:40am</td>
                <td>
                  <span className='label label-success'>payment confirmed</span>
                </td>
                <td>
                  <span className='label label-warning'>preparing order</span>
                </td>
                <td>$37.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#3</a>
                  </Link>
                </th>
                <td>Feb 2, 08:30am</td>
                <td>
                  <span className='label label-success'>payment confirmed</span>
                </td>
                <td>
                  <span className='label label-info'>order shipped</span>
                </td>
                <td>$37.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#4</a>
                  </Link>
                </th>
                <td>Feb 2, 10:00am</td>
                <td>
                  <span className='label label-default'>awaiting confirmation</span>
                </td>
                <td>
                  <span className='label label-default'>pending</span>
                </td>
                <td>$50.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#3</a>
                  </Link>
                </th>
                <td>Feb 2, 08:30am</td>
                <td>
                  <span className='label label-success'>payment confirmed</span>
                </td>
                <td>
                  <span className='label label-info'>order shipped</span>
                </td>
                <td>$37.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#4</a>
                  </Link>
                </th>
                <td>Feb 2, 10:00am</td>
                <td>
                  <span className='label label-default'>awaiting confirmation</span>
                </td>
                <td>
                  <span className='label label-default'>pending</span>
                </td>
                <td>$50.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#3</a>
                  </Link>
                </th>
                <td>Feb 2, 08:30am</td>
                <td>
                  <span className='label label-success'>payment confirmed</span>
                </td>
                <td>
                  <span className='label label-info'>order shipped</span>
                </td>
                <td>$37.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#4</a>
                  </Link>
                </th>
                <td>Feb 2, 10:00am</td>
                <td>
                  <span className='label label-default'>awaiting confirmation</span>
                </td>
                <td>
                  <span className='label label-default'>pending</span>
                </td>
                <td>$50.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#3</a>
                  </Link>
                </th>
                <td>Feb 2, 08:30am</td>
                <td>
                  <span className='label label-success'>payment confirmed</span>
                </td>
                <td>
                  <span className='label label-info'>order shipped</span>
                </td>
                <td>$37.00</td>
              </tr>
              <tr>
                <th scope='row'>
                  <Link href='/vendor/order/order-id'>
                    <a>#4</a>
                  </Link>
                </th>
                <td>Feb 2, 10:00am</td>
                <td>
                  <span className='label label-default'>awaiting confirmation</span>
                </td>
                <td>
                  <span className='label label-default'>pending</span>
                </td>
                <td>$50.00</td>
              </tr>
            </tbody>
          </Table>
        </div>

        <Pagination>
          <PaginationItem>
            <PaginationLink previous href='#' />
          </PaginationItem>
          <PaginationItem active>
            <PaginationLink href='#'>
              1
            </PaginationLink>
          </PaginationItem>
          <PaginationItem>
            <PaginationLink href='#'>
              2
            </PaginationLink>
          </PaginationItem>
          <PaginationItem>
            <PaginationLink href='#'>
              3
            </PaginationLink>
          </PaginationItem>
          <PaginationItem>
            <PaginationLink href='#'>
              4
            </PaginationLink>
          </PaginationItem>
          <PaginationItem>
            <PaginationLink href='#'>
              5
            </PaginationLink>
          </PaginationItem>
          <PaginationItem>
            <PaginationLink next href='#' />
          </PaginationItem>
        </Pagination>
      </div>
    )
  }
}

export default OrderList
