import React from 'react'
import GoogleMapReact from 'google-map-react'
import {
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap'
import ImageUpload from 'components/common/image-upload'
import SearchBox from 'components/common/mapSearchBox'
import config from 'config'

class AddProduct extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      mapsApiLoaded: false,
      mapInstance: null,
      mapsapi: null,
      places: [],
      product: []
    }
  }

  apiHasLoaded = (map, maps) => {
    this.setState({
      mapApiLoaded: true,
      mapInstance: map,
      mapApi: maps
    })
  }

  addPlace = (place) => {
    console.log(place)
    this.setState({ places: place })
  }

  render () {
    const {
      mapApiLoaded, mapInstance, mapApi
    } = this.state

    return (
      <div className='form-wrapper'>
        <Form>
          <FormGroup>
            <Label for='product_name'>Title</Label>
            <Input type='text' name='product[name]' id='product_name' placeholder='Enter product title' />
          </FormGroup>
          <FormGroup>
            <Label for='product_description'>Description</Label>
            <Input type='textarea' name='product[description]' id='product_description' rows='5' />
          </FormGroup>
          <FormGroup>
            <Label for='product_description'>Images</Label>
            <ImageUpload />
          </FormGroup>
          <FormGroup>
            <Label for='product_price'>Price</Label>
            <Input type='text' name='product[price]' id='product_price' placeholder='Example: $23.00' />
          </FormGroup>
          <FormGroup>
            <Label for='product_size'>Panel Location</Label>
            <div className='map-container'>
              {mapApiLoaded && <SearchBox map={mapInstance} mapApi={mapApi} addplace={this.addPlace} />}

              <GoogleMapReact
                bootstrapURLKeys={{
                  key: config.googleMapApiKey,
                  libraries: ['places', 'geometry']
                }}
                defaultCenter={this.props.center}
                defaultZoom={this.props.zoom}
                yesIWantToUseGoogleMapApiInternals
                onGoogleApiLoaded={({ map, maps }) => {
                  this.apiHasLoaded(map, maps)
                }}
              />
            </div>
          </FormGroup>
          <FormGroup>
            <Label for='product_info'>Additional Information</Label>
            <Input type='textarea' name='product[info]' id='product_info' rows='5' />
          </FormGroup>
          <div className='form-action'>
            <button className='btn-dark btn-submit'>Submit</button>
          </div>
        </Form>
      </div>
    )
  }
}

/**
 * Default Props
 */
AddProduct.defaultProps = {
  selected: [],
  center: {
    lat: 59.95,
    lng: 30.33
  },
  zoom: 11
}

export default AddProduct
