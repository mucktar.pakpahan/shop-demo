import React from 'react'

class Categories extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <ul>
        <li>
          <a href='#' className='active'>
            All
          </a>
        </li>

        <li>
          <a href='#'>
            Women
          </a>
        </li>

        <li>
          <a href='#'>
            Men
          </a>
        </li>

        <li>
          <a href='#'>
            Kids
          </a>
        </li>

        <li>
          <a href='#'>
            Accesories
          </a>
        </li>
      </ul>
    )
  }
}

/**
 * Default Props
 */
Categories.defaultProps = {
  selected: []
}

export default Categories
