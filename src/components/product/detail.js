import React from 'react'
import Link from 'next/link'
import { Collapse } from 'reactstrap'
import GoogleMapReact from 'google-map-react'
import ProductImages from './images'
import config from 'config'

class Detail extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      collapseLocation: true,
      collapseInfo: false,
      collapseReview: false
    }
  }

  toggleContent = (selected) => {
    console.log('selected', selected)
    this.setState({
      [selected]: !this.state[selected]
    })
  }

  increaseCartQuantity = () => {

  }

  decreaseCartQuantity = () => {

  }

  render () {
    return (
      <div className='container product-detail'>
        <div className='detail-wrapper'>

          <ProductImages />

          <div className='product-detail-description respon5'>
            <h4 className='product-detail-name'>
              Boxy T-Shirt with Roll Sleeve Detail
            </h4>

            <span className='product-detail-price'>
              $22/kw
            </span>

            <p className='product-detail-title'>
              Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.
            </p>

            <div className='product-detail-options'>
              <div className='options-detail'>
                <div className='detail-title'>Owner :</div>

                <Link href='#'>
                  <a>
                    John Doe
                  </a>
                </Link>
              </div>
            </div>

            <div className='dropdown-content-wrapper'>
              <h5 className='dropdown-content-toggle' onClick={() => this.toggleContent('collapseLocation')}>
                Panel Location
                {
                  this.state.collapseLocation
                    ? <i className='down-mark fa fa-minus dis-none' aria-hidden='true' />
                    : <i className='up-mark fa fa-plus' aria-hidden='true' />
                }
              </h5>
              <Collapse isOpen={this.state.collapseLocation} className='dropdown-content panel-location'>
                <div className='panel-location'>
                  <GoogleMapReact
                    bootstrapURLKeys={{ key: config.googleMapApiKey }}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                  >
                    <div>Product Location</div>
                  </GoogleMapReact>
                </div>
              </Collapse>
            </div>

            <div className='dropdown-content-wrapper'>
              <h5 className='dropdown-content-toggle' onClick={() => this.toggleContent('collapseInfo')}>
                Additional information
                {
                  this.state.collapseInfo
                    ? <i className='down-mark fa fa-minus dis-none' aria-hidden='true' />
                    : <i className='up-mark fa fa-plus' aria-hidden='true' />
                }
              </h5>
              <Collapse isOpen={this.state.collapseInfo} className='dropdown-content'>
                <p>
                  Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat
                </p>
              </Collapse>
            </div>

            <div className='dropdown-content-wrapper'>
              <h5 className='dropdown-content-toggle' onClick={() => this.toggleContent('collapseReview')}>
                Reviews (0)
                {
                  this.state.collapseReview
                    ? <i className='down-mark fa fa-minus dis-none' aria-hidden='true' />
                    : <i className='up-mark fa fa-plus' aria-hidden='true' />
                }
              </h5>

              <Collapse isOpen={this.state.collapseReview} className='dropdown-content'>
                <p>
                  Fusce ornare mi vel risus porttitor dignissim. Nunc eget risus at ipsum blandit ornare vel sed velit. Proin gravida arcu nisl, a dignissim mauris placerat
                </p>
              </Collapse>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

/**
 * Default Props
 */
Detail.defaultProps = {
  center: {
    lat: 59.95,
    lng: 30.33
  },
  zoom: 11
}

export default Detail
