import React from 'react'
import Categories from './categories'
import FilterPrice from './price'

class Filter extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='leftbar'>
        <h4 className='categories'>
          Categories
        </h4>

        <Categories />

        <h4 className='filters'>
          Filters
        </h4>

        <FilterPrice />
      </div>
    )
  }
}

/**
 * Default Props
 */
Filter.defaultProps = {
  selected: []
}

export default Filter
