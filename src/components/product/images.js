import React from 'react'
import Slider from 'react-slick'

const settings = {
  customPaging: function (i) {
    return (
      <a>
        <img src={`/static/images/thumb-item-0${i + 1}.jpg`} />
      </a>
    )
  },
  dots: true,
  arrows: false,
  fade: true,
  autoplay: false,
  dotsClass: 'slick-dots slick-thumb',
  infinite: true,
  speed: 500,
  autoplaySpeed: 6000,
  slidesToShow: 1,
  slidesToScroll: 1
}

class Item extends React.Component {
  constructor (props) {
    super(props)

    this.state = {}
  }

  render () {
    return (
      <div className='detail-images respon5'>
        <div className='wrap-slider'>

          <Slider {...settings}>
            <div className='slide-image'>
              <img src='/static/images/product-detail-01.jpg' alt='IMG-PRODUCT' />
            </div>

            <div className='slide-image'>
              <img src='/static/images/product-detail-02.jpg' alt='IMG-PRODUCT' />
            </div>

            <div className='slide-image'>
              <img src='/static/images/product-detail-03.jpg' alt='IMG-PRODUCT' />
            </div>
          </Slider>
        </div>
      </div>
    )
  }
}

export default Item
