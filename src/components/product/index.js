import React from 'react'
import Router from 'next/router'
import ProductItem from './item'

class Products extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      page: 1,
      limit: 10
    }
  }

  addProductToCart = () => {
    Router.push('/checkout')
  }

  addProductToWishlist = () => {

  }

  loadProducts = () => {

  }

  render () {
    return (
      <div className='row'>
        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-02.jpg'
            name='Herschel supply co 25l'
            price='$75.00'
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-03.jpg'
            name='Denim jacket blue'
            price='$92.50'
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-05.jpg'
            name='Coach slim easton black'
            price='$165.90'
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-07.jpg'
            name='Frayed denim shorts'
            price='$29.50'
            isSale
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-01.jpg'
            name='Herschel supply co 25l'
            price='$75.00'
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-07.jpg'
            name='Frayed denim shorts'
            price='$29.50'
            isSale
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-03.jpg'
            name='Denim jacket blue'
            price='$92.50'
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>

        <div className='item-wrapper col-sm-12 col-md-6 col-lg-6'>
          <ProductItem
            image='/static/images/item-05.jpg'
            name='Coach slim easton black'
            price='$165.90'
            addToCart={this.addProductToCart}
            addToWishlist={this.addProductToWishlist}
          />
        </div>
      </div>
    )
  }
}

/**
 * Default Props
 */
Products.defaultProps = {
  sort: 'default'
}

export default Products
