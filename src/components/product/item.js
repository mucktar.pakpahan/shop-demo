import React from 'react'

class Item extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isSaved: props.isSaved
    }
  }

  render () {
    const labelClass = this.props.isSale ? 'card-sale' : 'card-new'

    return (
      <div className='card'>
        <div className={`card-img ${labelClass}`}>
          <img src={this.props.image} alt={this.props.name} />

          <div className='card-overlay card-action'>
            <a href='#' className='btn-addwishlist'>
              <i className='icon-wishlist icon_heart_alt' aria-hidden='true' />
              <i className='icon-wishlist icon_heart' aria-hidden='true' />
            </a>

            <div className='btn-addcart'>
              <button className='btn-dark btn-add' onClick={this.props.addToCart}>
                Add to Cart
              </button>
            </div>
          </div>
        </div>

        <div className='card-txt'>
          <a href='/product/slug' className='product-name'>
            {this.props.name}
          </a>

          <span className='card-price'>
            {this.props.price}
          </span>
        </div>
      </div>
    )
  }
}

/**
 * Default Props
 */
Item.defaultProps = {
  isSale: false,
  isSaved: false
}

export default Item
