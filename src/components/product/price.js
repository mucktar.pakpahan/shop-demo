import React from 'react'
import { Range } from 'rc-slider'

import 'rc-slider/assets/index.css'

class Price extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      lowerBound: 20,
      upperBound: 40,
      value: [20, 40]
    }
  }

  onLowerBoundChange = (e) => {
    this.setState({ lowerBound: +e.target.value })
  }

  onUpperBoundChange = (e) => {
    this.setState({ upperBound: +e.target.value })
  }

  onSliderChange = (value) => {
    this.setState({
      value
    })
  }

  handleApply = () => {
    const { lowerBound, upperBound } = this.state
    this.setState({ value: [lowerBound, upperBound] })
  }

  render () {
    return (
      <div className='filter-price'>
        <div className='filter-price-title'>
          Price
        </div>

        <div className='wra-filter-bar'>
          <div id='filter-bar' />
          <Range allowCross={false} value={this.state.value} onChange={this.onSliderChange} />
        </div>

        <div className='filter-price-result'>
          <div className='btn-filter'>
            <button className='btn-dark'>
              Filter
            </button>
          </div>

          <div className='detail s-text3'>
            Range: $<span id='value-lower'>610</span> - $<span id='value-upper'>980</span>
          </div>
        </div>
      </div>
    )
  }
}

/**
 * Default Props
 */
Price.defaultProps = {
  range: []
}

export default Price
