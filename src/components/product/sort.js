import React from 'react'
import Select from 'react-select'

const options = [
  { value: null, label: 'Default' },
  { value: 'popular', label: 'Popularity' },
  { value: 'price-asc', label: 'Price: low to high' },
  { value: 'price-desc', label: 'Price: high to low' }
]

class Sort extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      selectedOption: props.selected
    }
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption })
  }

  render () {
    const { selectedOption } = this.state

    return (
      <div className='products-header'>
        <div className='sort-wrap'>
          <Select
            instanceId='sort_product'
            className='sort'
            value={selectedOption}
            onChange={this.handleChange}
            options={options}
            placeholder='Sort By'
          />
        </div>

        <span className='s-text8 p-t-5 p-b-5'>
          Showing 1–12 of 16 results
        </span>
      </div>
    )
  }
}

/**
 * Default Props
 */
Sort.defaultProps = {
  selected: null,
  total: 0
}

export default Sort
