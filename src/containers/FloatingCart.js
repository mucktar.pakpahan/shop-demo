import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Map } from 'immutable'
import { connect } from 'react-redux'

import { getTopRepos } from 'actions/repos'
import HeaderCart from 'components/header/cart'

class FloatingCart extends Component {
  static async getInitialProps ({ store, query }) {
    let lang = query.lang || 'javascript'
    await store.dispatch(getTopRepos({ lang }))
  }

  componentDidMount () {
    let { getTopRepos } = this.props
    getTopRepos({ lang: 'ruby' })
  }

  render () {
    return (
      <Fragment>
        <HeaderCart />
      </Fragment>
    )
  }
}

function mapStateToProps (state) {
  return {
    repos: state.repos
  }
}

FloatingCart.propTypes = {
  repos: PropTypes.instanceOf(Map).isRequired,
  getTopRepos: PropTypes.func.isRequired
}

export { FloatingCart }
export default connect(mapStateToProps, {
  getTopRepos
})(FloatingCart)
